XParser
=======

Parse any text by an XML syntax definitions. Result is an XML.

Egy tetszőleges szövegfájlt elemez a megadott XML alapú szintaxis alapján, és generál ez alapján egy XML eredményfájlt.
Az eredményfájl szerkezete alapvetően az illesztési szabályok csomópontjain alapul.

### Dokumentáció / Documentation

- [http://uhisoft.hu/xparser/](http://uhisoft.hu/xparser/)

## Használati példa / Example
```php
    $syntax = file_get_contents('syntax.xml');
    $p = new XParser_File($fh, $syntax);
    $r = $p->apply();
    fclose($fh);
```

## A szintaxis fájl szerkezete / Metasyntax

```xml
<syntax>
	<!-- match -->
	<match name="rulename" />
	
	<!-- rule definitons -->
	<rule name="rulename">
		<match rule="rulename" name="nodename"><!-- matches rule and optionally generates new node --></match>
		<return><!-- matches and generates output --></return>
		<list min max><!-- matches zero or more times --></list>
		<char><!-- matches one char (PCRE) --></char>
		<term><!-- matches string literally, not PCRE --></term>
		<or><!-- matches if exist matching subtag --></or>
		<opt><!-- list min=0 max=1 --></opt>
		<case>: same as [list min="1" max="1"]. Useful for grouping, e.g in 'or' tag.</case>
		<node name>: Generates a named node with matched content tag sequence (implicit rule). If content fails, node is not generating.</node>
		<attr name [value]>: Generates attribute from matched content's text (chars and terms). If content fails, attribute is not generating.</attr>
		<any> other tag: similar to [match rule] with rule=nodename, but without any output, not even if contains 'node'.</any>
		<value [select]>returns literal value</value>
		<temp>evaluates content, but after that, the input pointer will be restored.</temp>
		<peek name [length]>writes input contents with given length (default 15) into trace output</peek>
	</rule>
</syntax>
```

## Tervezett funkciók / Future plans

- <with-param name [select]>sends a local variable to matching rule with literal value. {} expressions are evaluated</with-param>
- select attributes: evaluates expression. $var retrieves variable value 
- <var name [select]>creates local variable</var>
	