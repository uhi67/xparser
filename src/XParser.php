<?php /** @noinspection PhpUnused */

namespace uhi67\xparser;

use DOMElement;
use Exception;
use uhi67\uxml\UXMLDoc;
use uhi67\uxml\UXMLElement;

/**
 * The XParser module
 *
 * @author Peter Uherkovich (uherkovich.peter@gmail.com)
 * @copyright 2014-2020
 * @license MIT
 * @package modules
 */

/**
 * Exception occurred in Parser
 */
class XParserException extends Exception {

}

/**
 * Parser based on XML syntax definitions.
 * Use descendants of abstract Parser
 * @see XParser_File
 */
abstract class XParser {
	/** @var UXMLDoc $syntax */
	private $syntax;
	private $cut;
	/** @var UXMLDoc $result */
	public $result;
	public $debug;

	function __construct($syntax) {
		$this->cut = [];
		$this->syntax = new UXMLDoc();
		$this->syntax->loadXML($syntax);
		$this->result = new UXMLDoc();
		$this->result->formatOutput = true;
	}
	/**
	 * Must return a char or false on EOF
	 * @return string
	 */
	abstract function getc();

	abstract function ungetc($c);

	abstract function getPos();

	abstract function setPos($p);

	/**
	 * Applies syntax on input.
	 * Finds default match in syntax file and applies getSymbol(name) of it.
	 * Builds XML representation structure
	 * <defaultname>
	 *    <submatch1>...</submatch1>
	 *    ...
	 * </defaultname>
	 *
	 * @return UXMLElement result node or null
	 * @throws XParserException
	 */
	function apply() {
		$defaultnodes = $this->syntax->documentElement->selectNodes("match");
		if($defaultnodes->length!=1) throw new XParserException('Must be exactly one main match node.');
		$name = $defaultnodes->item(0)->getAttribute('name');
		$result_node = $this->getSymbol($name);
		return $result_node;
	}

	/**
	 * Tries to read a symbol by named rule.
	 * Inserts result into result structure. Returns result node or false on failure
	 *
	 * @param String $name
	 *
	 * @return UXMLElement -- result node
	 * @throws XParserException
	 */
	function getSymbol($name) {
		$this->cut = [];

		/** @var UXMLElement $node_symbol */
		$node_symbol = $this->result->createElement($name);
		if(!$this->result->documentElement) $node_symbol = $this->result->appendChild($node_symbol);
		else $node_symbol = $this->result->documentElement->appendChild($node_symbol);

		if(!$this->matchRule($name, $node_symbol)) return null;
		$this->joinAttr($node_symbol);
		return $node_symbol;
	}

	/**
	 * tries to match a rule once (several rule versions or-ed)
	 *
	 * @param string $name -- rule name
	 * @param DOMElement|\DOMDocumentFragment $node_parent -- inserts result tree here on success
	 *
	 * @return boolean -- false on fail (tree result will be untouched)
	 * @throws XParserException
	 */
	function matchRule($name, $node_parent) {
		array_unshift($this->cut, false);
		$rules = $this->syntax->documentElement->selectNodes("rule[@name='$name']");
	 	$r = false;
	 	/** @var DOMElement $rule */
		foreach ($rules as $rule) {
			if($this->matchSeq($rule, $node_parent, 0)) {$r=true; break;}
			if($this->cut[0]) {
				// Hibakezelés?
				break;
			}
		}
		array_shift($this->cut);
		return $r;
	}

	/**
	 * matches a sequence of patterns under the node, and collects the result tree.
	 * succeeds only if all of members suceeded consecutively
	 *
	 * @param DOMElement $rule -- evaluate all children of rule node
	 * @param DOMElement|\DOMDocumentFragment $node_parent -- inserts results here
	 * @param int $f -- add chars to result
	 *
	 * @return boolean -- false if failed
	 * @throws Exception
	 * @throws XParserException
	 */
	function matchSeq($rule, $node_parent, $f=0) {
		$resultFragment = $this->result->createDocumentFragment();
		$p = $this->getPos();
		$r = true;

		if(!$rule->firstChild) {
			return true;
		}

		forEach($rule->childNodes as $node) {
			if($node->nodeType != XML_ELEMENT_NODE) continue;
			$r &= $this->matchNode($node, $resultFragment, $f);
			if(!$r) break;
		}

		if($r) {
			if($node_parent && $resultFragment->hasChildNodes()) {
				if($resultFragment->childNodes->length==1
						&& $resultFragment->firstChild->nodeType==XML_TEXT_NODE
						&& $node_parent->lastChild
						&& $node_parent->lastChild->nodeType==XML_TEXT_NODE) {
			 		$node_parent->lastChild->nodeValue .= $resultFragment->firstChild->nodeValue;
		 		}
				else $node_parent->appendChild($resultFragment);
			}
		}
		else {
			$this->setPos($p);
		}
		return $r;
	}

	/**
	 * matches a node, and collects the result tree.
	 * used in matchSequence and matchOr
	 *
	 * @param DOMElement $node -- syntax node
	 * @param DOMElement|\DOMDocumentFragment $resultFragment -- inserts results here
	 * @param int $f -- add chars to result
	 *
	 * @return boolean -- false if failed
	 * @throws XParserException
	 */
	function matchNode($node, $resultFragment, $f=0) {
		$r = true;
		switch($node->nodeName) {
			case 'node':
				$name = $node->getAttribute('name');
				/** @var DOMElement $node_submatch */
				$node_submatch = $resultFragment->appendChild($this->result->createElement($name));
				$r &= $this->matchSeq($node, $node_submatch, $f);
				$this->joinAttr($node_submatch);
				break;
			case 'return':
				$r &= $this->matchSeq($node, $resultFragment, 1);
				break;
			case 'attr':
				$v = null;
				$name = $node->getAttribute('name');
				if(!$name) throw new XParserException('Invalid attribute name');
				if($value = $node->getAttribute('value')) {
					$v = $value;
				}
				else {
					$res1 = $this->result->createDocumentFragment();
					$r &= $this->matchSeq($node, $res1, 0);
					if($r) {
						$this->filterNodes($res1, '_attr');
						$v = $res1->textContent;
					}
				}
				if($v!==null) {
					$attrnode = $this->result->createElement('_attr');
					$attrnode->setAttribute('name', $name);
					$attrnode->appendChild($this->result->createTextNode($v));
					$resultFragment->appendChild($attrnode);
				}
				break;
			case 'value':
				$value = $node->textContent;
				if($f) {
					$lastChild = $resultFragment->lastChild;
					if(!$lastChild || $lastChild->nodeType != XML_TEXT_NODE) $lastChild = $resultFragment->appendChild($this->result->createTextNode(''));
					$lastChild->nodeValue =  $lastChild->nodeValue . $value;
				}
				break;
			case 'char':
				$re = $node->nodeValue;
				$c = $this->getc();
				try {
					if (!preg_match(self::toRegExp($re), $c)) {
						$this->ungetc($c);
						$r = false;
					}
				}
				catch(Exception $e) {
					$msg = $e->getMessage();
					throw new Exception("Ipvalid pattern `~$re~`: $msg", 0, $e);
				}
				// Append to result
				if($r && $f) {
					$lastChild = $resultFragment->lastChild;
					while($lastChild && $lastChild->nodeType != XML_TEXT_NODE) $lastChild = $lastChild->previousSibling;
					if(!$lastChild || $lastChild->nodeType != XML_TEXT_NODE) $lastChild = $resultFragment->appendChild($this->result->createTextNode(''));
					$lastChild->nodeValue =  $lastChild->nodeValue . $c;
				}
				break;
			case 'term':
				$term = $node->nodeValue;
				$this->matchRule('s', null);
				$w = $this->getTerm($term);
				if($w) $this->matchRule('s', null);
				if($w===false) $r = false;
				if($r && $f) {
					$lastChild = $resultFragment->lastChild;
					if(!$lastChild || $lastChild->nodeType != XML_TEXT_NODE) $lastChild = $resultFragment->appendChild($this->result->createTextNode(''));
					$lastChild->nodeValue =  $lastChild->nodeValue . $term;
				}
				break;
			case 'list':	// default list min="0" max="inf"
				$min = (int)$node->getAttribute('min');
				$max = (int)$node->getAttribute('max');
				$r &= $this->matchList($node, $min, $max, $resultFragment, $f);
				break;
			case 'opt':		// same as list max="1"
		 		$r &= $this->matchList($node, 0, 1, $resultFragment, $f);
				break;
			case 'case':	// same as list min=1 max="1"
		 		$r &= $this->matchList($node, 1, 1, $resultFragment, $f);
				break;
			case 'or':		// elements or-ed
		 		$r &= $this->matchOr($node, $resultFragment, $f);
				break;
			case 'match':
				$rule = $node->getAttribute('rule');
				$name = $node->getAttribute('name');
				if($rule=='' && $name!='') $rule = $name;
				if($rule=='' && $name=='') throw new XParserException("invalid match in syntax");
				if($name!='') $resultnode = $resultFragment->appendChild($this->result->createElement($name));
				else $resultnode = $resultFragment;
				$r &= $this->matchRule($rule, $resultnode);
				if($name!='') $this->joinAttr($resultnode);
				break;
			case 'cut':
				$this->cut[0] = true;
				break;
			case 'temp':
				// TODO: kiértékeli (mint case), outputot is generál ha kell, de a végén az input pointert visszaállítja.
				$p = $this->getPos();
		 		$r &= $this->matchList($node, 1, 1, $resultFragment, $f);
				$this->setPos($p);
				break;
			case 'peek':
				break;
			default:
				$rules = $this->syntax->documentElement->selectNodes("rule[@name='$node->nodeName']");
				if($rules->length != 0) {
					$r &= $this->matchRule($node->nodeName, $resultFragment);
				}
				else throw new XParserException("invalid node '$node->nodeName' in syntax");
				break;
		}
		return $r;
	}

	/**
	 * 'list' matches if sequence matches [min, max] times.
	 * Zero match terminates first time.
	 *
	 * @param DOMElement $node
	 * @param int $min
	 * @param int $max
	 * @param DOMElement $node_parent
	 * @param boolean $f -- add chars to result
	 *
	 * @return boolean
	 * @throws XParserException
	 */
	function matchList($node, $min, $max, $node_parent, $f) {
		$i=0;
		$p = $this->getPos();
		$r = true;
		while($max==0 || $i<$max) {
			$p1 = $this->getPos();
			$r1 = $this->matchSeq($node, $node_parent, $f);
			$p2 = $this->getPos();
			if($i>1000 || !$r1 || $p1==$p2) break;
			$i++;
		}
		if($i<$min) $r = false;
		if(!$r) {
			$this->setPos($p);
		}
		return $r;
	}

	/**
	 * 'or' matches if any of elements matches. (Use 'case' for group elements)
	 * Matching element terminates early.
	 *
	 * @param DOMElement $node
	 * @param DOMElement|\DOMDocumentFragment $node_parent
	 * @param boolean $f -- add chars to result
	 *
	 * @return boolean
	 * @throws XParserException
	 */
	function matchOr($node, $node_parent, $f) {
		$p = $this->getPos();
		$r = false;
		$node1 = $node->firstChild;
		while($node1) {
			$resultFragment = $this->result->createDocumentFragment();
			if($node1->nodeType == XML_ELEMENT_NODE) {
				$r |= $this->matchNode($node1, $resultFragment, $f);
				if($r) {
					if($node_parent && $resultFragment->hasChildNodes()) {
						if($resultFragment->childNodes->length==1
								&& $resultFragment->firstChild->nodeType==XML_TEXT_NODE
								&& $node_parent->lastChild
								&& $node_parent->lastChild->nodeType==XML_TEXT_NODE) {
					 		$node_parent->lastChild->nodeValue .= $resultFragment->firstChild->nodeValue;
				 		}
						else $node_parent->appendChild($resultFragment);
					}
					break;
				}
				else {
					$this->setPos($p);
				}
			}
			$node1 = $node1->nextSibling;
		}
		return $r;
	}

	function getTerm($term) {
		$p = $this->getPos();
		$s = '';
		for($i=0;$i<strlen($term);$i++) {
			$c = $this->getc();
			$s.=$c;
			if($c!=substr($term,$i,1)) {
				$this->setPos($p);
				return false;
			}
		}
		return true;
	}

	/**
	 * A megadott node tartalmát XML stringgé alakítja
	 *
	 * @param DOMElement $node
	 * @param integer $ent -- 0:default, 1:htmlentities, 2:formatted
	 * @param string $c -- színes span-ben adja vissza
	 *
	 * @return string -- az XML string
	 * @throws Exception
	 */
	function export($node, $ent=0,$c='') {
		if($ent==2) {
			$node->ownerDocument->formatOutput = false;
			$node->ownerDocument->preserveWhiteSpace = false;
		}
		if(!$node) $r='null';
		else {
			if(!$node->ownerDocument) throw new Exception('Érvénytelen DOM');
			$r = $node->ownerDocument->saveXML($node);
		}
		if($ent==1) $r = htmlentities($r);
		if($c) $r = /** @lang text */'<span style="color:'.$c.'">'.$r.'</span>';
		return $r;
	}

	/**
	 * Converts <_attr name>value</_attr> subnodes to attributes.
	 * Duplicate attributes overwrite existing ones.
	 * <_attr> subnodes without name attribute are be ignored
	 * All other attributes and sub-subnodes are ignored
	 * All <_attr> subnodes will be completely removed (including the irregular ones)
	 *
	 * @param DOMElement $node
	 * @return void
	 */
	function joinAttr($node) {
		$child = $node->firstChild;
		while($child) {
			$next = $child->nextSibling;
			if($child->nodeName=='_attr') {
				$name = $child->getAttribute('name');
				$value = $child->textContent;
				if($name) $node->setAttribute($name, $value);
				$node->removeChild($child);
			}
			$child = $next;
		}
	}

	/**
	 * Törli a megadott nevő gyermek node-okat.
	 * Elsősorban az _attr node-ok eltávolítására szolgál, ahol a text tartalom kell csak.
	 *
	 * @param mixed $node
	 * @param mixed $nodename
	 * @return void
	 */
	function filterNodes($node, $nodename) {
		$child = $node->firstChild;
		while($child) {
			$next = $child->nextSibling;
			if($child->nodeName==$nodename) {
				$node->removeChild($child);
			}
			$child = $next;
		}
	}

	/**
	 * Creates a delimited RegEx pattern from a bare pattern.
	 *
	 * @param $pattern -- pattern without delimiter
	 * @return string -- pattern with ~ delimiters and escaped inner ~s
	 */
	public static function toRegExp($pattern) {
		return '~' . str_replace('~', '\~', $pattern) . '~';
	}

}
